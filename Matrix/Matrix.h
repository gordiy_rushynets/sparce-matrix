//
//  Matrix.h
//  Matrix
//
//  Created by Gordiy Rushynets on 5/27/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#ifndef Matrix_h
#define Matrix_h

#include <iostream>
#include "Node.h"

class Matrix
{
private:
    // first element
    Node** topRow;
    // first column
    Node** leftColumn;
    size_t rows;
    size_t columns;
    
    // print all zeros elements
    void printZeros(std::ostream& out, size_t start, size_t last) const;
    // add elements in matrix
    void addElement(size_t rowIndex, size_t columnIndex, double data);
    static double calculateScalar(const Matrix& matrix1, const Matrix& matrix2, size_t rowIndex, size_t columnIndex);
    void deleteMatrix();
    void copyMatrix(const Matrix& matrix);
    
public:
    Matrix();
    Matrix(size_t rows, size_t columns);
    Matrix(const Matrix& matrix);
    ~Matrix();
    
    // get value of element in matrix
    double getValue(size_t rowIndex, size_t columnIndex) const;
    // set value of element
    void setValue(size_t rowIndex, size_t columnIndex, double value);

    // overload operators for making operations with matrix
    Matrix operator+(const Matrix& matrix) const;
    Matrix operator-(const Matrix& matrix) const;
    Matrix operator*(const Matrix& matrix) const;
    
    Matrix& operator=(const Matrix& matrix);
    
    friend std::istream& operator>>(std::istream& in, Matrix& matrix);
    friend std::ostream& operator<<(std::ostream& out, const Matrix& matrix);
};

#endif /* Matrix_h */

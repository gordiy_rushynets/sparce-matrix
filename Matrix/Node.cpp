//
//  Node.cpp
//  Matrix
//
//  Created by Gordiy Rushynets on 5/27/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#include <stdio.h>
#include  "Node.h"

// defult constructor
Node::Node() :
nodeData(0),
rightElement(nullptr),
bottomElement(nullptr),
rowIndex(0),
columnIndex(0)
{ }

// create constructor where all elements have pointer to right and bottom elem
Node::Node(size_t row, size_t column) :
nodeData(0),
rightElement(nullptr),
bottomElement(nullptr),
rowIndex(row),
columnIndex(column)
{ }

// create constructor where we set value of data
Node::Node(size_t row, size_t column, double data) :
nodeData(data),
rightElement(nullptr),
bottomElement(nullptr),
rowIndex(row),
columnIndex(column)
{ }

// must return value of data
double Node::data() const
{
    return nodeData;
}

// set value of data
void Node::setData(double newData)
{
    nodeData = newData;
}
// return row index
size_t Node::row() const
{
    return rowIndex;
}

// set row
void Node::setRow(size_t newRow)
{
    rowIndex = newRow;
}

size_t Node::column() const
{
    return columnIndex;
}

void Node::setColumn(size_t newColumn)
{
    columnIndex = newColumn;
}

// return pointer to right element
Node* Node::right() const
{
    return rightElement;
}

// set pointer to right elem
void Node::setRight(Node* newRight)
{
    rightElement = newRight;
}

Node* Node::bottom() const
{
    return bottomElement;
}

void Node::setBottom(Node* newBottom)
{
    bottomElement = newBottom;
}

// overload operator
std::ostream &operator<<(std::ostream &out, const Node &node)
{
    return out << node.nodeData;
}

//
//  Matrix.cpp
//  Matrix
//
//  Created by Gordiy Rushynets on 5/27/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#include <stdio.h>
#include <fstream>
#include "Matrix.h"

// default constructor
Matrix::Matrix() :
rows(0),
columns(0),
topRow(nullptr),
leftColumn(nullptr)
{ }


Matrix::Matrix(size_t rows, size_t columns) :
rows(rows),
columns(columns)
{
    // create first element
    topRow = new Node*[columns];
    // crete first row
    leftColumn = new Node*[rows];
    
    // initialize all elements in matrix pushing in elem pointer to Node
    for (size_t i = 0; i < columns; i++)
    {
        topRow[i] = new Node();
    }
    
    for (size_t i = 0; i < rows; i++)
    {
        leftColumn[i] = new Node();
    }
}
// create new matrix where each elemt has link to elem other matrix
Matrix::Matrix(const Matrix& matrix)
{
    copyMatrix(matrix);
}

Matrix::~Matrix()
{
    deleteMatrix();
}

void Matrix::addElement(size_t rowIndex, size_t columnIndex, double data)
{
    if (rowIndex > 0 && columnIndex > 0 && data != 0)
    {
        // create new NODE
        Node* node = new Node(rowIndex, columnIndex, data);
        // create copy which equal to left column
        Node* copy = leftColumn[rowIndex - 1];
        
        // if right elem is not null_pointer and copy index last right elem in column not equal to column index
        if (copy->right() != nullptr && copy->right()->column() != columnIndex)
        {
            
            while (copy->right()->column() < columnIndex && copy->right()->row() != 0)
            {
                // create last right elem
                copy = copy->right();
            }
            
            if (copy->right()->column() == columnIndex && copy->right()->row() == rowIndex)
            {
                return;
            }
            
            // last right element
            Node* copyRight = copy->right();
            
            // set right elem with pointer
            copy->setRight(node);
            //
            node->setRight(copyRight);
        }
        else
        {
            // right elem are equal matrix
            copy->setRight(node);
            //
            node->setRight(leftColumn[rowIndex - 1]);
        }
        
        // create first row
        copy = topRow[columnIndex - 1];
        
        // if bottom is not last element in matrix
        if (copy->bottom() != nullptr && copy->bottom()->row() != rowIndex)
        {
            //
            while (copy->bottom()->row() < rowIndex && copy->bottom()->column() != 0)
            {
                copy = copy->bottom();
            }
            
            if (copy->bottom()->column() == columnIndex && copy->bottom()->row() == rowIndex)
            {
                return;
            }
            
            Node* copyBottom = copy->bottom();
            
            copy->setBottom(node);
            node->setBottom(copyBottom);
        }
        else
        {
            copy->setBottom(node);
            node->setBottom(topRow[columnIndex - 1]);
        }
    }
}

// get value of element
double Matrix::getValue(size_t rowIndex, size_t columnIndex) const
{
    // create pointer to first col
    Node* node = leftColumn[rowIndex - 1];
    
    if (rowIndex > 0 && columnIndex > 0 && node->right() != nullptr)
    {
        while (node->right()->column() < columnIndex && node->right() != leftColumn[rowIndex - 1])
        {
            node = node->right();
        }
        
        if (node->right()->column() == columnIndex)
        {
            return node->right()->data();
        }
    }
    
    return 0;
}

void Matrix::setValue(size_t rowIndex, size_t columnIndex, double value)
{
    if (value != 0)
    {
        if (rowIndex > 0 && columnIndex > 0)
        {
            // create pointer to first column
            Node* node = leftColumn[rowIndex - 1];
            // if right elem is not null pointer
            if (node->right() != nullptr)
            {
                //
                while (node->right()->column() < columnIndex && node->right() != leftColumn[rowIndex - 1])
                {
                    // overload node
                    node = node->right();
                }
                
                if (node->right()->column() == columnIndex)
                {
                    // set value for node
                    node->right()->setData(value);
                    return;
                }
            }
        }
        
        addElement(rowIndex, columnIndex, value);
    }
}

// multiplication
double Matrix::calculateScalar(const Matrix& matrix1, const Matrix& matrix2, size_t rowIndex, size_t columnIndex)
{
    double result = 0;
    
    for (int i = 1; i <= matrix1.columns; i++)
    {
        // multiplication matrix1 and matrix2
        result += matrix1.getValue(rowIndex, i) * matrix2.getValue(i, columnIndex);
    }
    
    return result;
}


Matrix Matrix::operator+(const Matrix& matrix) const
{
    Matrix result(rows, columns);
    
    for (size_t i = 0; i < rows; i++)
    {
        for (size_t j = 0; j < columns; j++)
        {
            result.setValue(i + 1, j + 1, getValue(i + 1, j + 1) + matrix.getValue(i + 1, j + 1));
        }
    }
    
    return result;
}

Matrix Matrix::operator-(const Matrix& matrix) const
{
    Matrix result(rows, columns);
    
    for (size_t i = 0; i < rows; i++)
    {
        for (size_t j = 0; j < columns; j++)
        {
            result.setValue(i + 1, j + 1, getValue(i + 1, j + 1) - matrix.getValue(i + 1, j + 1));
        }
    }
    
    return result;
}

Matrix Matrix::operator*(const Matrix& matrix) const
{
    Matrix result(rows, columns);
    
    for (size_t i = 1; i <= rows; i++)
    {
        for (size_t j = 1; j <= columns; j++)
        {
            result.setValue(i, j, calculateScalar(*this, matrix, i, j));
        }
    }
    
    return result;
}

// show zeros elem from first to last elem
void Matrix::printZeros(std::ostream& out, size_t start, size_t last) const
{
    for (size_t j = start; j < last; j++)
    {
        out << 0 << ' ';
    }
}

std::ostream& operator<<(std::ostream& out, const Matrix& matrix)
{
    // loop for rows
    for (size_t i = 0; i < matrix.rows; i++)
    {
        // if last elem is null pointer show 0
        if (matrix.leftColumn[i]->right() == nullptr)
        {
            matrix.printZeros(out, 0, matrix.columns);
        }
        else
        {
            // create copy pointer tp fitst elem in row
            Node* copy = matrix.leftColumn[i];
            size_t prevIndex = 0;
            
            // while last elem are not == prev elem
            while (copy->right() != matrix.leftColumn[i])
            {
                // curent index are equal index of column
                size_t currentIndex = copy->right()->column();
                
                // print elem in matrix
                matrix.printZeros(out, prevIndex + 1, currentIndex);
                out << copy->right()->data() << ' ';
                
                prevIndex = currentIndex;
                copy = copy->right();
                
                if (copy->right() == matrix.leftColumn[i])
                {
                    matrix.printZeros(out, currentIndex, matrix.columns);
                }
            }
        }
        
        out << std::endl;
    }
    
    return out;
}

std::istream& operator>>(std::istream& in, Matrix& matrix)
{
    double value;
    
    for (size_t i = 0; i < matrix.columns; i++)
    {
        for (size_t j = 0; j < matrix.columns; j++)
        {
            in >> value;
            
            if (value != 0)
            {
                matrix.addElement(i + 1, j + 1, value);
            }
        }
    }
    
    return in;
}

Matrix& Matrix::operator=(const Matrix& matrix)
{
    deleteMatrix();
    copyMatrix(matrix);
    
    return *this;
}

void Matrix::deleteMatrix()
{
    // loop for rows
    for (size_t i = 0; i < rows; i++)
    {
        // while first elem in col has not a pointer to yourself and pointer to last elem in col is not null ptr
        while (leftColumn[i]->right() != leftColumn[i] && leftColumn[i]->right() != nullptr)
        {
            // save pointer to pref col
            Node* copy1 = leftColumn[i];
            Node* copy2 = leftColumn[i];
            
            // shortening the line in copy1
            while (copy1->right() != leftColumn[i])
            {
                copy1 = copy1->right();
            }
            // shortening the line in copy2
            while (copy2->right() != copy1)
            {
                copy2 = copy2->right();
            }
            // remove copy1
            delete copy1;
            // create for next elem value of prev elem
            copy2->setRight(leftColumn[i]);
        }
    }
    // removing all element in col
    for (size_t i = 0; i < rows; i++)
    {
        delete leftColumn[i];
    }
    // removing main element in matrix
    for (size_t i = 0; i < columns; i++)
    {
        delete topRow[i];
    }
}

void Matrix::copyMatrix(const Matrix& matrix)
{
    rows= matrix.rows;
    columns = matrix.columns;
    // init first row in matrix
    topRow = new Node*[matrix.columns];
    // init first col in matrix
    leftColumn = new Node*[matrix.rows];
    
    // creating elements for matrix
    for (size_t i = 0; i < columns; i++)
    {
        topRow[i] = new Node();
    }
    
    for (size_t i = 0; i < rows; i++)
    {
        // create default first col
        leftColumn[i] = new Node();
        // create pointer for elements
        Node* matrixCopy = matrix.leftColumn[i];
        
        // pass the whole matrix
        while (matrixCopy->right() != matrix.leftColumn[i] && matrixCopy->right() != nullptr)
        {
            Node* node = new Node(i, matrixCopy->right()->column(), matrixCopy->right()->data());
            
            node->setRight(leftColumn[i]);
            node->setBottom(topRow[node->column() - 1]);
            Node* horizontalCopy = leftColumn[i];
            
            while (horizontalCopy->right() != leftColumn[i] && horizontalCopy->right() != nullptr)
            {
                horizontalCopy = horizontalCopy->right();
            }
            
            horizontalCopy->setRight(node);
            Node* verticalCopy = topRow[matrixCopy->right()->column() - 1];
            
            while (verticalCopy->bottom() != topRow[node->column() - 1] && verticalCopy->bottom() != nullptr)
            {
                verticalCopy = verticalCopy->bottom();
            }
            
            verticalCopy->setBottom(node);
            matrixCopy = matrixCopy->right();
        }
    }
}

//
//  Node.h
//  Matrix
//
//  Created by Gordiy Rushynets on 5/27/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#ifndef Node_h
#define Node_h
#include <iostream>

class Node
{
private:
    // element in matrix
    double nodeData;
    // link to right elem
    Node* rightElement;
    // link to left elem
    Node* bottomElement;
    // row's index
    int rowIndex;
    // column's index
    int columnIndex;
    
public:
    // constructors
    Node();
    Node(size_t row, size_t column);
    Node(size_t row, size_t column, double data);
    
    
    // default functions 
    double data() const;
    void setData(double newData);
    
    size_t row() const;
    void setRow(size_t newRow);
    
    size_t column() const;
    void setColumn(size_t newColumn);
    
    Node* right() const;
    void setRight(Node* newRight);
    
    Node* bottom() const;
    void setBottom(Node* newBottom);
    
    friend std::ostream &operator<<(std::ostream &out, const Node &node);
};

#endif /* Node_h */
